# ~*~ encoding: utf-8 ~*~
from flask import render_template, request, jsonify, Response, flash, redirect, \
    g
from flask.ext import excel
from flask.ext.login import login_user, logout_user, current_user, \
    login_required
from flask.helpers import url_for

from app import app, models, lm
from forms import LoginForm
from suggestions.db import RedisDatabase, Formatter
from suggestions.tasks import Task


@app.before_request
def before_request():
    g.user = current_user


@lm.user_loader
def user_loader(user_id):
    return models.User.query.filter_by(id=int(user_id)).first()


@app.route('/login', methods=['GET', 'POST'])
def login():
    if g.user is not None and g.user.is_authenticated():
        return redirect(url_for('index'))

    form = LoginForm()
    if form.validate_on_submit():
        username, password = form.username.data, form.password.data
        user = models.User.query.filter_by(
            username=username, password=password).first()
        if user is None:
            flash('User with such credentials not found')
            return redirect(url_for('login'))

        print(user)
        login_user(user, form.remember_me.data)
        return redirect(request.args.get('next') or url_for('index'))

    return render_template('login.html', title='Sign In', form=form)


@app.route('/logout', methods=['GET', 'POST'])
@login_required
def logout():
    logout_user()
    return redirect(url_for('index'))


@app.route('/add-task', methods=['POST'])
@login_required
def api_add_task():
    rdb = RedisDatabase.instance()
    try:
        keyword, depth, with_numbers = request.form.get('keyword', None), \
                                       request.form.get('depth', None), \
                                       request.form.get('with_numbers', False)
        Task.create_task(keyword, depth, with_numbers)
        rdb.add_root_keyword(keyword)
        return Response(status=201)

    except:
        import sys
        sys.stderr.write('ERROR ON POST, form {}'.format(request.form))
        return Response(status=500)


@app.route('/cancel-active-tasks', methods=['DELETE'])
@login_required
def api_cancel_active_tasks():
    rdb = RedisDatabase.instance()
    rdb.rdb.delete(rdb.task_queue, rdb.task_suggestion_queue)
    return Response(status=204)


@app.route('/search', methods=['POST'])
@login_required
def api_search():
    keyword = request.form.get('keyword', None)
    rdb = RedisDatabase.instance()
    return jsonify(dict(result=rdb.search_roots(keyword)))


@app.route('/view', methods=['POST'])
@login_required
def api_results():
    data = request.form
    return jsonify(dict(result=Formatter.get_dict(data)))


@app.route('/ready', methods=['POST'])
@login_required
def api_number_of_tasks():
    return jsonify(dict(tasks=RedisDatabase.instance().get_number_of_tasks()))


@app.route('/export')
@login_required
def download():
    data = Formatter.get_arrays()
    response = excel.make_response_from_array(data, 'csv')
    response.headers['Content-Disposition'] = \
        'attachment; filename="export.csv"'
    return response


@app.route('/', methods=['GET'])
@login_required
def index():
    return render_template('index.html')
